git_repo_path=$(dirname "$0")
git_repo_path=$(realpath "$git_repo_path")
source "$git_repo_path"/vars.sh

echo "========================================================================"
echo "Install nginx, apache, node_exporter"
echo "========================================================================"
# shellcheck disable=SC2154
echo "[INFO] Server IP: $nginx_ip"

# shellcheck disable=SC2154
# shellcheck disable=SC2087
ssh "$nginx_ip" << EOF
  mkdir -p "$git_repo_path"
EOF

scp -qr "$git_repo_path" "$nginx_ip":"$(dirname "$git_repo_path")"

# shellcheck disable=SC2087
ssh "$nginx_ip" << EOF
  cd $git_repo_path
  source vars.sh
  source ./scripts/install_nginx.sh
  install_nginx_func $git_repo_path
  cd $git_repo_path
  source ./scripts/install_monitoring.sh
  install_node_exporter $git_repo_path
  rm -rf $git_repo_path
EOF

echo "========================================================================"
echo "Install prometheus and grafana"
echo "========================================================================"
# shellcheck disable=SC2154
echo "[INFO] Server IP: $prometheus_ip"

# shellcheck disable=SC2087
# shellcheck disable=SC2154
ssh "$prometheus_ip" << EOF
  mkdir -p "$git_repo_path"
EOF

scp -qr "$git_repo_path" "$prometheus_ip":"$(dirname "$git_repo_path")"

# shellcheck disable=SC2087
ssh "$prometheus_ip" << EOF
  cd $git_repo_path
  source vars.sh
  source ./scripts/install_monitoring.sh
  install_prometheus $git_repo_path
  echo "[INFO] Grafana start installing"
  cd $git_repo_path
  source vars.sh
  source ./scripts/install_monitoring.sh
  install_grafana $git_repo_path
  rm -rf $git_repo_path
EOF

echo "========================================================================"
echo "Install mysql master"
echo "========================================================================"
# shellcheck disable=SC2154
echo "[INFO] Server IP: $master_ip"

# shellcheck disable=SC2154
# shellcheck disable=SC2087
ssh "$master_ip" << EOF
  mkdir -p "$git_repo_path"
EOF

scp -qr "$git_repo_path" "$master_ip":"$(dirname "$git_repo_path")"

# shellcheck disable=SC2087
ssh "$master_ip" << EOF
  cd $git_repo_path
  source vars.sh
  source ./scripts/install_mysql.sh
  install_mysql_func
  configure_mysql_master
EOF

echo "========================================================================"
echo "Copy master status to slave VM"
echo "========================================================================"

source "$git_repo_path"/scripts/install_mysql.sh
copy_status_from_master
copy_status_to_slave

echo "========================================================================"
echo "Install mysql slave"
echo "========================================================================"
# shellcheck disable=SC2154
echo "[INFO] Server IP: $slave_ip"

# shellcheck disable=SC2154
# shellcheck disable=SC2087
ssh "$slave_ip" << EOF
  mkdir -p "$git_repo_path"
EOF

scp -qr "$git_repo_path" "$slave_ip":"$(dirname "$git_repo_path")"

# shellcheck disable=SC2087
ssh "$slave_ip" << EOF
  cd $git_repo_path
  source vars.sh
  source ./scripts/install_mysql.sh
  install_mysql_func
  configure_mysql_slave
  echo "[INFO] Schedule mysql backup in cron"
  cp "$git_repo_path"/scripts/mysql_backup.sh /root
  chmod +x /root/mysql_backup.sh
  source vars.sh
  echo "0 0 * * * /root/mysql_backup.sh root '$mysql_root_pass'" > /root/root_crontab
  crontab -u root /root/root_crontab
  rm -rf $git_repo_path
EOF

echo "========================================================================"
echo "Deploy backed up tables into master"
echo "========================================================================"
# shellcheck disable=SC2154
echo "[INFO] Server IP: $master_ip"

# shellcheck disable=SC2087
ssh "$master_ip" << EOF
  cd $git_repo_path
  source vars.sh
  source ./scripts/install_mysql.sh
  deploy_backed_up_data_to_master
  rm -rf $git_repo_path
EOF