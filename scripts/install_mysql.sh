#execute on master and slave
install_mysql_func(){
  echo '[INFO] MYSQL: Disable firewalld'
  systemctl stop firewalld 2>/dev/null
  systemctl disable firewalld 2>/dev/null


  echo '[INFO] MYSQL: Add mysql repo'
  rpm -Uvh https://repo.mysql.com/mysql80-community-release-el7-6.noarch.rpm 1>/dev/null
  rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022 1>/dev/null
  sed -i 's/enabled=1/enabled=0/' /etc/yum.repos.d/mysql-community.repo

  echo '[INFO] MYSQL: Install mysql...'
  yum --enablerepo=mysql80-community install -q -y mysql-community-server 1>/dev/null

  echo '[INFO] MYSQL: Start mysql...'
  systemctl enable --now mysqld 2>/dev/null

  temp_pass=$(grep 'temporary password' /var/log/mysqld.log | rev | cut -d" " -f1 | rev)
  echo "[DEBUG] found string: $(grep 'temporary password' /var/log/mysqld.log)"
  echo "[DEBUG] detected temporary password: $temp_pass"

  echo "[INFO] MYSQL: Install expect..."
  yum install -q -y expect 1>/dev/null

  echo "[INFO] MYSQL: Execute mysql_secure_installation..."

  expect -c "
  set timeout 10
  spawn mysql_secure_installation -u root --use-default
  expect \"Enter password for user root:\" {send \"$temp_pass\r\"}
  expect \"New password:\" {send \"$temp_pass\r\"}
  expect \"Re-enter new password:\" {send \"$temp_pass\r\"}
  expect eof
  " 2>/dev/null 1>/dev/null

  echo '[INFO] MYSQL: Change root password for mysql...'
  # shellcheck disable=SC2154
  mysql -uroot -p"$temp_pass" -e \
  "ALTER USER 'root'@'localhost' IDENTIFIED WITH 'caching_sha2_password' BY '$mysql_root_pass'"

}

# execute on master
configure_mysql_master(){
  echo '[INFO] MYSQL: Configure master...'

  echo "server_id=1" >> /etc/my.cnf
  systemctl restart mysqld

  echo '[INFO] MYSQL: Create replica user...'
  # shellcheck disable=SC2154
  mysql --skip-column-names -uroot -p"$mysql_root_pass" -e \
  "CREATE USER $mysql_replica_user_name@'%' IDENTIFIED WITH 'caching_sha2_password' BY '$mysql_replica_user_pass'"

  mysql --skip-column-names -uroot -p"$mysql_root_pass" -e \
  "GRANT REPLICATION SLAVE ON *.* TO $mysql_replica_user_name@'%'"

  mysql --skip-column-names -uroot -p"$mysql_root_pass" -e \
  "SHOW MASTER STATUS" 1>/root/master_status
}

# execute on slave
configure_mysql_slave(){
  echo "[INFO] MYSQL: Configure mysql slave..."
  echo "server_id=2" >> /etc/my.cnf
  systemctl restart mysqld

  echo '[INFO] MYSQL: Parse master status...'
  binlog=$(awk '{ print $1 }' /root/master_status)
  position=$(awk '{ print $2 }' /root/master_status)

  echo '[INFO] MYSQL: Stop slave...'
  mysql --skip-column-names -uroot -p"$mysql_root_pass" -e \
  "STOP SLAVE"

  echo '[INFO] MYSQL: Change master...'
  # shellcheck disable=SC2154
  mysql --skip-column-names -uroot -p"$mysql_root_pass" -e \
  "CHANGE MASTER TO MASTER_HOST='$master_ip', MASTER_USER='$mysql_replica_user_name', MASTER_PASSWORD='$mysql_replica_user_pass', MASTER_LOG_FILE='$binlog', MASTER_LOG_POS=$position, GET_MASTER_PUBLIC_KEY = 1"

  echo '[INFO] MYSQL: Start slave...'
  mysql --skip-column-names -uroot -p"$mysql_root_pass" -e \
  "START SLAVE"

}

# execute on main
copy_status_from_master(){
  echo '[INFO] Copy master status from mysql master vm to orchestrator'
  scp "$master_ip":/root/master_status /root/master_status
}

# execute on main
copy_status_to_slave(){
  echo '[INFO] Copy master status from  to orchestrator to mysql slave'
  # shellcheck disable=SC2154
  scp /root/master_status "$slave_ip":/root/master_status
  rm -f /root/master_status
}

deploy_backed_up_data_to_master(){
  echo '[INFO] Deploy backed up tables to master'
  # shellcheck disable=SC2154
  cd "$sql_backup_dir" || return
  # shellcheck disable=SC2045
  for db in $(ls)
    do
      mysql -uroot -p"$mysql_root_pass" -e "CREATE DATABASE $db"
      for table in "$db"/*.sql; do mysql -uroot -p"$mysql_root_pass" -e "USE $db" < "$table"; done
    done

}