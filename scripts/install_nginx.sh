#!/usr/bin/bash


install_nginx_func(){
  git_repo_path="$1"
  source "$git_repo_path"/scripts/copy_config.sh

  echo '[INFO] Turn off selinux...'
  "$git_repo_path"/scripts/selinux_script.sh --set_status 0 --set_config_status disabled 1>/dev/null

  echo '[INFO] Stop and disable firewalld...'
  systemctl stop firewalld 2>/dev/null
  systemctl disable firewalld 2>/dev/null

  echo '[INFO] Install epel repository...'
  yum install -q -y epel-release 1>/dev/null
  echo '[INFO] yum clean all...'
  yum clean all 1>/dev/null
  echo '[INFO] yum makecache...'
  yum makecache 1>/dev/null

  echo '[INFO] Install nginx...'
  yum install -q -y nginx 1>/dev/null

  echo '[INFO] Install Apache...'
  yum install -q -y httpd 1>/dev/null

  echo '[INFO] Create dir /var/www/server_8281...'
  mkdir -p /var/www/server_8281
  echo "OneOneOne" > /var/www/server_8281/index.html

  echo '[INFO] Create dir /var/www/server_8282...'
  mkdir -p /var/www/server_8282
  echo "TwoTwoTwo" > /var/www/server_8282/index.html

  copy_func "$git_repo_path"/configs/apache/httpd.conf
  copy_func "$git_repo_path"/configs/apache/my_apache_servers.conf
  copy_func "$git_repo_path"/configs/nginx/nginx.conf
  copy_func "$git_repo_path"/configs/nginx/balancer.conf

  echo '[INFO] Start Nginx...'
  systemctl enable nginx 2>/dev/null
  systemctl restart nginx

  echo '[INFO] Start Apache...'
  systemctl enable httpd 2>/dev/null
  systemctl restart httpd

  echo '[INFO] Nginx and Apache are installed, configured and started'

}