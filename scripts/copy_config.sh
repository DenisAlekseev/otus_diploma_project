copy_func(){
  path_to_file="$1"
  location=$(head -n1 "$path_to_file")
  location=${location/\#/}
  echo "[INFO] Copy $path_to_file to $location"
  \cp "$path_to_file" "$location" 1>/dev/null
}