install_prometheus(){
  git_repo_path="$1"

  echo '[INFO] Prometheus: Stop and disable firewalld...'
  systemctl stop firewalld 2>/dev/null
  systemctl disable firewalld 2>/dev/null

  echo '[INFO] Turn off selinux...'
  "$git_repo_path"/scripts/selinux_script.sh --set_status 0 --set_config_status disabled 1>/dev/null

  echo '[INFO] Prometheus: Download prometheus'
  # shellcheck disable=SC2154
  curl --request GET -sL \
       --url "$prometheus_tar_gz_url"\
       --output "prometheus.tar.gz"

  echo '[INFO] Prometheus: Extract archive'
  mkdir prometheus
  tar -xf 'prometheus.tar.gz' -C prometheus --strip-components=1
  rm -f 'prometheus.tar.gz'

  echo '[INFO] Prometheus: Create user "prometheus"...'
  useradd --no-create-home --shell /usr/sbin/nologin prometheus

  echo '[INFO] Prometheus: Copy binary and config files...'
  mkdir {/etc/,/var/lib/}prometheus 1>/dev/null
  \cp prometheus/prometheus prometheus/promtool /usr/local/bin/ 1>/dev/null
  \cp -r prometheus/consoles prometheus/console_libraries /etc/prometheus/ 1>/dev/null

  # Copy yml config with replacing IP from vars.sh
  ( echo "cat <<EOF >/etc/prometheus/prometheus.yml";
  cat "$git_repo_path"/configs/prometheus/prometheus.yml;
  echo "EOF";
  ) >temp.yml
  . temp.yml
  rm -f temp.yml 1>/dev/null

  echo '[INFO] Prometheus: Change owner for config and binary files'
  chown -R prometheus: /etc/prometheus 1>/dev/null
  chown -R prometheus: /var/lib/prometheus 1>/dev/null
  chown prometheus: /usr/local/bin/prom{tool,etheus} 1>/dev/null

  source "$git_repo_path"/scripts/copy_config.sh
  copy_func "$git_repo_path"/configs/prometheus/prometheus.service
  systemctl daemon-reload 1>/dev/null
  systemctl enable --now prometheus
  echo '[INFO] Prometheus installed, configured and started'
}

install_grafana(){
  git_repo_path="$1"
  echo "[INFO] Download grafana rpm"
  # shellcheck disable=SC2154
  curl --request GET -sL \
       --url "$grafana_rpm_url"\
       --output 'grafana.rpm'

  echo "[INFO] Install grafana"
  yum install -q -y grafana.rpm 1>/dev/null

  echo "[INFO] Start and enable grafana"
  systemctl enable --now grafana-server 2>/dev/null
  echo '[INFO] Grafana installed and started. Configure it via browser!!!'
}

install_node_exporter(){
  git_repo_path="$1"

  echo '[INFO] Download node_exporter'
  # shellcheck disable=SC2154
  curl --request GET -sL \
       --url "$node_exporter_tar_gz_url"\
       --output "node_exporter.tar.gz"

  echo '[INFO] Extract archive'
  mkdir node_exporter
  tar -xf 'node_exporter.tar.gz' -C node_exporter --strip-components=1
  rm -f 'node_exporter.tar.gz'

  echo '[INFO] Create user "node_exporter"...'
  useradd --no-create-home --shell /usr/sbin/nologin node_exporter

  echo '[INFO] Copy binary file node_exporter...'
  \cp node_exporter/node_exporter /usr/local/bin/

  echo '[INFO] Change owner for binary file'
  chown -v node_exporter: /usr/local/bin/node_exporter

  source "$git_repo_path"/scripts/copy_config.sh
  copy_func "$git_repo_path"/configs/prometheus/node_exporter.service

  systemctl daemon-reload
  systemctl enable --now node_exporter 2>/dev/null
  echo '[INFO] node_exporter installed and started'
}