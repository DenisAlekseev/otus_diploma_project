#!/usr/bin/bash

possible_selinux_statuses='Enforcing|Permissive|1|0'
possible_config_statuses='disabled|permissive|enforcing'
script_name=$0

usage(){
  pattern="%-20s %-s\n"
  printf "Description:\n"
  printf " Display info about SElinux status. Switch SElinux statuses\n"
  printf '\nUsage:\n'
  printf " %s -h | --help\n" "$script_name"
  printf " %s -s | --status\n" "$script_name"
  printf " %s OPTION ARG ...\n" "$script_name"
  printf "\nOptions:\n"
  printf "$pattern" "-h, --help" "Print this message"
  printf "$pattern" "-s, --status" "Print selinux and selinux config statuses"
  printf "$pattern" "--set_status" "Set temporary (until reboot) SELinux status [$possible_selinux_statuses]"
  printf "$pattern" "--set_config_status" "Change SELinux status in config [$possible_config_statuses]"
  printf "\nCodes:\n"
  pattern="%-4s %-s\n"
  printf "$pattern" "1" "Unknown flag provided"
  printf "$pattern" "2" "Not 'root' user tries to change SELinux status"
  printf "$pattern" "3" "Unknown SELinux mode provided for setting"
  printf "$pattern" "4" "Unknown SELinux config mode provided for setting"
  printf "$pattern" "5" "No flag provided"
}

error_usage(){
  printf "Try '$script_name -h' or '$script_name --help' to find out how to use the script\n" >&2
}

statuses(){
  sestatus | grep -P --color=never "SELinux status:|Current mode:|Mode from config file:"
}

check_root(){
  if [ "$(whoami)" != 'root' ]
  then
    printf 'You must be "root" for changing SELinux status\n' >&2
    exit 2
  fi
}

if [ -z "$1" ]
then
  printf "No flag provided\n"  >&2
  error_usage
  exit 5
fi

options=$(getopt -n "otus_selinux_script" -o hs -l "help,status,set_status:,set_config_status:" -- "$@")
eval set -- "$options"

usage=
show_statuses=
selinux_status=
config_status=
while true
do
  case $1 in
  -h | --help ) usage=usage; shift; break ;;
  -s | --status ) show_statuses=show; shift; break ;;
  --set_status ) selinux_status=$2; check_root; shift 2;;
  --set_config_status ) config_status=$2; check_root; shift 2;;
  -- ) shift; break ;;
  * ) printf "Unknown flag.\n"; error_usage; exit 1 ;;
  esac
done

if [ -n "$usage" ]
then
  usage
  exit 0
fi

if [ -n "$show_statuses" ]
then
  statuses
  exit 0
fi

if [ -n "$selinux_status" ]
then
  if [[ "$selinux_status" =~ ^$possible_selinux_statuses$ ]]
  then
    setenforce "$selinux_status"
    printf "SELinux 'Current mode' was changed to\n"
    statuses | grep --color=never "Current mode:"
  else
    printf "Unknown SELinux mode \"%s\"\n" "$selinux_status" >&2
    printf "Possible values are: %s\n" "$possible_selinux_statuses" >&2
    exit 3
  fi
fi


if [ -n "$config_status" ]
then
  if [[ "$config_status" =~ ^$possible_config_statuses$ ]]
  then
    sed -i -E --follow-symlinks "s/^SELINUX=[a-z]+$/SELINUX=${config_status}/" /etc/sysconfig/selinux
    printf "SELinux config was updated to\n"
    grep -P --color=never "^SELINUX=" /etc/sysconfig/selinux
    printf "You should reboot the machine.\n"
    printf "Reboot now [y/n]?: "
    read answer
    if [ "$answer" = "y" ]
    then
      reboot now
    else
      printf "Answer is either 'no' or incorrect. Reboot the machine manually.\n"
    fi
  else
    printf "Unknown SELinux config mode \"%s\"\n" "$config_status" >&2
    printf "Possible values are: %s\n" "$possible_config_statuses" >&2
    exit 4
  fi
fi
exit 0