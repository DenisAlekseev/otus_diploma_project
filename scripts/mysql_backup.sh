#!/usr/bin/env bash

#read -rp 'Enter mysql user: ' user
#read -srp 'Enter mysql password: ' password
user="$1"
password="$2"
printf '\n'
MYSQL="mysql --skip-column-names -u$user -p$password"
MYSQL_STATUS="mysql -u$user -p$password"
temp_log='temp_log'

exec 2>$temp_log

print_statuses(){
  printf '\n'
  printf 'Current slave statuses:\n'
  $MYSQL_STATUS -e "SHOW SLAVE STATUS\G" | grep -Po 'Slave_SQL_Running_State:.*'
  $MYSQL_STATUS -e "SHOW SLAVE STATUS\G" | grep -Po 'Slave_IO_Running:.*'
  $MYSQL_STATUS -e "SHOW SLAVE STATUS\G" | grep -Po 'Slave_SQL_Running:.*'
  printf '\n'
}

print_statuses

printf 'STOP SLAVE\n'
$MYSQL -e "STOP SLAVE"

print_statuses

# shellcheck disable=SC2164
cd /root
for db in $($MYSQL -e "SHOW DATABASES LIKE '%\_db'")
do
  printf 'Create dir \"dump/%s\"\n\n' "$db"
  mkdir -p "dump/$db"
  printf 'Dump tables from DB \"%s\":\n' "$db"
  for table in $($MYSQL -e "USE $db; SHOW TABLES")
  do
    printf '%s\n' "$table"
    mysqldump -u"$user" -p"$password" --add-drop-table --add-locks \
    --create-options --disable-keys --extended-insert --single-transaction \
    --quick --set-charset --events --routines --triggers --source-data=2 \
    "$db" "$table" > "dump/$db/$table.sql"
  done
  printf 'All tables in DB "%s" were dumped\n\n' "$db"

done
printf 'All databases with name pattern "%%\_db" were dumped into "dump" dir\n'
printf '\nSTART SLAVE\n'
$MYSQL -e "START SLAVE"

print_statuses

grep -v 'Using a password on the command line interface can be insecure' "$temp_log" | sort | uniq > temporary_log
rm -f $temp_log
if [ -n "$(cat temporary_log)" ]
then
  printf '\nERRORS DURING SCRIPT EXECUTION:\n'
  cat temporary_log
  rm -f temporary_log
  exit 1
fi
rm -f temporary_log
exit 0
